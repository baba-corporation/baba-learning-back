# coding: utf-8
from fastai.vision import *
import warnings
warnings.filterwarnings('ignore')
from flask import Flask
from flask_cors import CORS
from flask import request
import urllib.request as req
import soundfile as sf
from werkzeug.utils import secure_filename
from werkzeug.datastructures import  FileStorage

app = Flask(__name__)

learnImage = load_learner('models/', 'export.pkl')
#learnSound = load_learner('models/', 'export.pkl')

CORS(app)

@app.route('/image')
def imagePredicter():


#récupérer l'url envoyé par le front

    imgUrl = request.args.get('url')
    req.urlretrieve(imgUrl, "image_name.jpg")
    
    img = open_image("image_name.jpg")
    ypredImage = learnImage.predict(img)

    #si 0 = france
    #si 1 = italie
    #si 2 = pays-bas

    return str(ypredImage[1].item())


@app.route('/sound', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
      f = request.files['file']
      f.save(secure_filename(f.filename))
      return 'file uploaded successfully'    