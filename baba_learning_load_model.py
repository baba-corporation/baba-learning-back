#!/usr/bin/env python
# coding: utf-8

# In[1]:


from fastai.vision import *
import warnings
warnings.filterwarnings('ignore')


# In[41]:


#data = ImageDataBunch.from_folder(path="brand", train='brand\\train', test='brand\\test', valid_pct=0.25, ds_tfms=get_transforms(), size=224, bs=8).normalize(imagenet_stats)
#data


# In[42]:


#learn = cnn_learner(data, models.resnet34, metrics=accuracy)


# In[44]:


#learn.load('brand\\models\\tmp',strict=False,remove_module=True)
learn = load_learner('brand\\models', '4epoch')


# In[50]:


#img = image uploadée  open_image('brand\\tmp\\fr.jpg')
img = open_image('brand\\tmp\\fr.jpg')
ypred = learn.predict(img)
category = data.classes[ypred[1].item()]
print(category)

