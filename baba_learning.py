#!/usr/bin/env python
# coding: utf-8

# In[32]:


from fastai.vision import *
import warnings
warnings.filterwarnings('ignore')


# In[31]:


download_images(urls="brand/france.txt",dest="brand/train/france",max_pics=200,max_workers=1)
download_images(urls="brand/italie.txt",dest="brand/train/italie",max_pics=200,max_workers=1)


# In[33]:


classes = ['france','italie']
for c in classes:
    verify_images("brand/train/"+c, delete=True)


# In[36]:


data = ImageDataBunch.from_folder(path="brand", train='train', test='test', valid_pct=0.25, ds_tfms=get_transforms(), size=224, bs=8).normalize(imagenet_stats)
# data


# In[37]:


learn = cnn_learner(data, models.resnet34, metrics=accuracy)


# In[38]:


learn.lr_find()


# In[22]:


learn.recorder.plot(suggestion=True)


# In[23]:


learn.fit_one_cycle(6, max_lr=slice(1e-03))


# In[29]:


img = open_image('brand/test/photo_fr.jpg')
ypred = learn.predict(img)
category = data.classes[ypred[1].item()]
print(category)


# In[ ]:


learn.export("4epoch")
# learn.unfreeze()

